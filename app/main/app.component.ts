import {Component} from 'angular2/core';

@Component({
  selector: 'seed-app',
  template: `
    <br>
    <div class="container">
      <div class="jumbotron">
        <h1 class="display-3">{{test}}</h1>
        <p class="lead">Seed repository, clone and ready to go!</p>
        <hr class="m-y-2">
        <p>Sebastián Gaviria</p>
      </div>
    </div>
    `,
  directives: []
})

export class AppComponent {
    public test = 'Angular 2 Working!';

  constructor() { }
}
