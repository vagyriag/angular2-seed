import 'reflect-metadata';
import 'zone.js';

import 'rxjs/Rx';


import {bootstrap} from 'angular2/platform/browser';
import {provide, ComponentRef} from 'angular2/core';
import {ROUTER_PROVIDERS} from 'angular2/router';

import {AppComponent} from './main/app.component';


bootstrap(AppComponent, [
  ROUTER_PROVIDERS
]);