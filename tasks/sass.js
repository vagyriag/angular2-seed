var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var batch = require('gulp-batch');
var watch = require('gulp-watch');
// var plumber = require('gulp-plumber');
var rimraf = require('gulp-rimraf');
var runSequence = require("run-sequence");

var paths = {
  src: 'app/styles',
  dist: 'dist/styles',
  fonts: '/fonts'
};

var tasks = {
  watch: 'watch-styles',
  clean: 'clean-styles',
  copy: 'copy-fonts',
  build: 'build-styles'
}

gulp.task(tasks.watch, function() {
  runSequence(tasks.build, function(error) {
    watch(paths.src + '/**/*.scss', (event) => gulp.start(tasks.build));
    watch(paths.src + paths.fonts + '/**/*', (event) => gulp.start(tasks.copy));
  })
}
);

// Delete the dist directory
gulp.task(tasks.clean, function() {
  return gulp.src(paths.dist, { read: false }).pipe(rimraf({ force: true }));
});

// copy fonts to the dist folder
gulp.task(tasks.copy, function() {
  return gulp.src(paths.src + paths.fonts + '/**').pipe(gulp.dest(paths.dist + paths.fonts));
});

// Compiles SASS > CSS
gulp.task(tasks.build, function() {
  return gulp.src(paths.src + '/styles.scss')
    // .pipe(plumber({errorHandler: err => { console.log(err); this.emit('end'); }}))
    // .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    // .pipe(sourcemaps.write('./stylesheets/maps'))
    .pipe(autoprefixer())
    .pipe(gulp.dest(paths.dist));
});